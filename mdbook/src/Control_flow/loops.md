# Loops
A loop consists of a statement or block called the "loop body".

The "loop body" is executed multiple times depending on a given condition.

Loops are used to represent a method  of executing a statement, block, or algorithm multiple times.

### FOR Loop
For loops utilize a control structure with three components written as expressions:

    initialization (starting point)
    tests the controlling expression
    make adjustments at the end of each loop (i.e. incrementation)

Three components in the head of the *for loop* define the loops actions and number of iterations executed by the loop.

Any of the three components may be omitted but the place-holders must remain there.

    in the words of the infamous Charles Petzold (a WINDOWS PIONEER)
    "three or more? use a for!"

### FOR loops execute blocks of code multiple times:
```c
///for loop syntax///

/*
expression1 is the initial-statement, commonly “i = 0”
expression2 is the condition, evaluated as a boolean expression
expression3 is the iteration-statement, typically increment
*/

for (expression1; expression2; expression3)
{
	statement1;	// Executed when expression2 != 0
}

/* 
Sequence of for loop execution:
	1.  Initialize expression1
	2.  Is expression2 true?
		2.a.  If so,
			2.a.i   execute the code block
			2.a.ii  execute expression3
			2.a.iii go back to 2.
		2.b.  If not, stop looping 
*/

///for loop example///
int i = 0;

for (i = 0; i < 15; i++)
{
    printf("i = %d\n", i + 1);
}
/*
executes the printf statment for all values of i
between 0 and 14, inclusive
*/

///for loop output///
i = 1
i = 2
i = 3
i = 4
i = 5
i = 6
i = 7
i = 8
i = 9
i = 10
i = 11
i = 12
i = 13
i = 14
i = 15
```
## SEE DEMO LAB 5

/*///THREE OR MORE? USE A FOR!///
This lab is more of a thought experiment...
Looking back, what previous labs could have benefited from a 
FOR loop? Were there...arrays to manipulate?...repetitious calculations...
and or places where code was copy-pasta?*/

## COMPLETE PERFORMANCE LAB 15

