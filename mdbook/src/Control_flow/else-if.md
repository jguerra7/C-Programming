#ELSE-IF statements

An if statement may also include multiple else conditions.

else-if  statements tests an if expression as TRUE or FALSE.

When the expression is TRUE, the if code block will be executed.

When the expression is FALSE, the first else-if expression will be tested in turn.

Else-if expressions are tested for TRUE in turn.

The else code block will be executed when the if and all else-if conditions are evaluated to false.

An if statement may also check multiple conditions.

```c
///else-if statement syntax///

if (expression1)
{
    statement1;
}
else if (expression2)
{
    statement2;
}
else
{
    statement3;
}

```

ELSE-IF example:

```c
///else-if statement examples///

if (i > u)
{
    printf("I is greater than u. \n");
}
else if (i < u)
{
    printf("u is greater than i. \n");
}
else
{
    printf("u and i are equal. \n");
}

```

NOTE: Do not leave else-if statements unwrapped {}!

## DEMO LAB 3: WHATS THE DIFFERENCE?

* Initialize ONLY three **int** variables to 0.
* Safely scan user input into variables #1 and #2 utilizing a single line.
* Using a single ELSE-IF statement:
    * If variable #1 is equal to variable #2, assign 0 to variable #3.
    * Otherwise, subtract the smallest from the largest (i.e. 3-2, 42-(-45), (-11)-(-1337), etc...) and assign the result to variable #3.
* Print the value of variable #3 if it is positive otherwise print and ERROR.

---

## PERFORMANCE LAB 13

