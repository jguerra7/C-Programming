# Structs

### This topic covers:

* Coding Style Guide
* Stub Code
* Definition
* Format
* Arrays of Structs
* Struct Visualization
* Linked Lists
* Function Pointers
* Circular Lists
