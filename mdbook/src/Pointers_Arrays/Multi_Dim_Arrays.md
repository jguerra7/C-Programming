# Multi Dimensional Arrays

## Multi-Dimensional meaning; more than one dimension

* Arrays can have more than one dimension
* Less common than arrays of pointers but...
* Similar to arrays of pointers

2D![](assets/2db.png)   3D![](assets/3db.jpg)   4D![](assets/4da.gif)
---
* Declare a multi-dimensional array
```
<data type> <variable name>[<row>][<column>];          // 2D array
<data type> <variable name>[<plane>][<row>][<column>]; // 3D array
```
* Examples
  * Two-dimensional double array with 4 rows, 6 columns
```
double dbaseTable1[4][6];
```
  * Three-dimensional integer array with 3 planes of 4 rows and 4 columns, each 
```
int threedimChessBoard[3][4][4];
```
  * Four-dimensional integer array of 20 Tri-D Chess Boards
```
int TriDTournament[20][3][4][4];
```

---
* Initialize a multi-dimensional array (Examples)
  * Two-dimensional double array with 4 row, 6 columns
```
double dbaseTable1[4][6] = {
	{ 1.1, 1.2, 1.3, 1.4, 1.5, 1.6},
	{ 2.1, 2.2, 2.3, 2.4, 2.5, 2.6},
	{ 3.1, 3.2, 3.3, 3.4, 3.5, 3.6},
	{ 4.1, 4.2, 4.3, 4.4, 4.5, 4.6}
};
// Each of dbaseTable1’s 4 elements are a dimension 6 float array
```
  * Three-dimensional integer array with 3 planes of 4 rows and 4 columns, each
```
int threeDimChessBoard[3][4][4] = {0};    //zeroized
```

---
* Initialize a multi-dimensional array (Examples)
  * Two-dimensional double array with 3 rows, 4 columns
```
double numberArray[3][4] = {\
    0.0, 0.1, 0.2, 0.3, \
    1.0, 1.1, 1.2, 1.3, \
    2.0, 2.1, 2.2, 2.3, \
};

/* Each index in numberArray's two dimensional matrix is filled, in order, 
starting with numberArray[0][0] */
```
![](assets/power1.png)

---
* Initialize a multi-dimensional array (Examples)
  * Two-dimensional double array with “ROWS” rows and “COLUMNS” columns
```
#define ROWS 6
#define COLUMNS 7

    int numberArray[ROWS][COLUMNS] = { 0 };
    int i = 0;	  // Iterate through rows
    int j = 0;	 // Iterate through columns

for (i = 0; i < ROWS; i++)
{
    for (j = 0; j < COLUMNS; j++)  
    {
        numberArray[i][j] = (i * 10) + j;
    }
}
```
![](assets/power2.png)

---
# Demonstration Lab
## Multi Dimensional Arrays

**You place your battleship!**

![](assets/Batt1.jpg)

* Use two two-dimensional arrays to replicate a single player’s game piece from 
Hasbro’s board game “Battleship”
* Initialize one of the arrays with your ship placement
* Print both arrays to stdout in a human-readable format

---
## Multi-Dimensional Arrays by reference

```
char theDreamIsReal[][] = {
    {"Leo"}, {"Chris"}, {"Ellen"}, {"Tom"}, {"Joe"}
};
```

|**Expression**|**Resolution**|
|:---|:---|
|theDreamIsReal|0x00CE9710|
|theDreamIsReal + 2|0x00CE971C|
|*(theDreamIsReal + 2)|Ellen|
|*(theDreamIsReal + 3)|0x00CE9713|
|*(*(theDreamIsReal + 2) + 3) |e|

---

* Memory storage of arrays is still contiguous, regardless of the **dimension**
* The following code zeroizes and then fills a two-dimensional array of integers

```
/* Example code will be executed per line and displayed in memory */
someIntArray[3][4] = { 0 };
int row = 0; int col = 0; int value = 0; // Forgive this faux pas

for (row = 0; row < 3; row++)
{
    for (col = 0; col < 4; col++)
    {
        *(*(someIntArray + row) + col) = value;
        value++;
    }
}
```

---

```
/* Example code will be executed per line and displayed in memory */
```
![](assets/MDA1.png)

---

```
someIntArray[3][4] = {0};
```
![](assets/MDA2.png)

---

```
int row = 0; int col = 0; int value = 0; // Forgive this faux pas
```
![](assets/MDA3.png)

---

```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA4.png)

---
```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA5.png)

---

```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA6.png)

---

```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA7.png)

---

```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA8.png)

---

```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA9.png)

---
```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA10.png)

---
```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA11.png)

---

```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA12.png)

---

```
*(*(someIntArray + row) + col) = value;
```
![](assets/MDA13.png)

---

* Memory storage of arrays is still contiguous, regardless of the **dimension**
* The following code zeroizes and then fills a two-dimensional array of integers

```
/* Example code will be executed per line and displayed in memory */
someIntArray[3][4] = { 0 };
int row = 0; int col = 0; int value = 0; // Forgive this faux pas

for (row = 0; row < 3; row++)
{
    for (col = 0; col < 4; col++)
    {
        *(*(someIntArray + row) + col) = value;
        value++;
    }
}
```
# Demonstration Lab

## Multi Dimensional Arrays

### "You sunk my battleship!"

![](assets/Batt1.jpg)

* Expand on DL I.5.a-6 
* Define pass-by-reference functions to replicate some aspect of the Hasbro game of Battleship
* print_grid()
* place_ships()
* verify_empty()
* FIRE()
* under_fire()

## Complete Performance Lab TicTacToe

