# Assert vs Errno

### Assert
(check for errors that should never happen)

* Valid range of values
* Specific values
* Design by contract
* non-NULL pointers (when the pointer can't be NULL)

### Errno
(Or other error handling techniques)

* Returns from system calls and library functions
* User input data
* Network input data
* File input data
* Out of the system resources or memory

# I/O Functions

I/O functions typically return useful values

Many I/O functions update ERRNO

### DEMO LAB - INPUT

Use appropriate error handling (assert vs errno) to handle I/O errors.

Replicate the error to test your implementations.

```c

#define _CRT_SECURE_NO_WARNINGS 1
#ifndef _DEBUG
#define NDEBUG
#endif
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>


//#define STEP1

#define BUF_SIZE 16

int main(void)
{
	char * inputString = (char *)calloc(BUF_SIZE, sizeof(char));
	assert(inputString);			// Assert that inputString is not NULL

#ifdef STEP1
	// Do step1 stuff here
	fgets(inputString, BUF_SIZE, stdin);
#else
	// Do other non-step1 stuff here
	scanf("%s", inputString);
#endif

	assert(inputString[BUF_SIZE - 1] == '\0');
	puts(inputString);

	return 0;
}

---
### COMPLETE PERFORMANCE LAB - OUTPUT
