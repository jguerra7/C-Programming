## Debugging Strategies:



* Study the system for the larger duration in order to understand the system. It helps debugger to construct different representations of systems to be debugging depends on the need. Study of the system is also done actively to find recent changes made to the software.

* Backwards analysis of the problem which involves tracing the program backward from the location of failure message in order to identify the region of faulty code. A detailed study of the region is conducting to find the cause of defects.

* Forward analysis of the program involves tracing the program forwards using breakpoints or print statements at different points in the program and studying the results. The region where the wrong outputs are obtained is the region that needs to be focused to find the defect.

* Using the past experience of the software debug the software with similar problems in nature. The success of this approach depends on the expertise of the debugger.

# Debugging notes

* Using `printf` statements to view the value(s) of variables is referred to as "poor man's" debugging
* Using a proper *debugger* is better
* A debugger is a program that simulates/runs another program and allows you to:
    * pause the execution
    * View variable values
    * "step" through the program line by line
    * set break points in a program and continue execution up to that point