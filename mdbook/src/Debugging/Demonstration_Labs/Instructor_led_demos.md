# How to Debug

There are a handful of situations that are common to debugging problems. The following sections will describe each of these situations and go through an example debugging session. We will approach the debugging problem from the perspective of a programmer: we witness a symptom or some observed bad behavior on the part of a program. We then present a technique to locate the problem in the program code and, ultimately, to identify the cause of the program error.

## Program crashes 

When a program stops executing in an unexpected manner, it is said to have crashed. Something went wrong, and the system was unable to continue running the program. For example, suppose the following code is contained in a file called crash.c

```c
	/* This program crashes.  The crash point can be found
	** using a debugger. */

#include <stdio.h>

main()
{
int x,y;

y=54389;
for (x=10; x>=0; x--)
  y=y/x;
printf("%d\n",y);
}

```
At the shell, we compile and execute the program, only to find that it crashes:
```
90cos@lesson> gcc -o crash1 crash1.c 
90cos@lesson> crash1 Floating exception (core dumped) 
90cos@lesson>
```
The error message “Floating exception” gives only a limited idea of what went wrong, and almost no idea of where it went wrong. The system has created a core dump file to help the programmer. It contains a snapshot of the contents of memory and other information about the system right at the moment that the program crashed. However, core dump files are usually large, containing far more than is needed for common debugging. Typically, core dump files are used only in advanced system programming problems.


A naive programmer might open up the C code file and begin studying the code, looking for possible sources of error. In a program as small as our example, this might even work. However, using a debugger is far simpler and will save a great deal of time. The idea is to run the program in the debugger until it crashes, and at that point look at what happened

```
90cos@lesson> gcc -g -o crash1 crash1.c 
90cos@lesson> gdb crash1 
(gdb) run 
Starting program: /home/ahoover/crash1 
Program received signal SIGFPE, Arithmetic exception. 
0x0804848b in main () at crash1.c:10
10	y=y/x; 
(gdb)
```
The debugger tells us that the program crashed at line 10, and shows us the line of 
code at line 10. Looking at that line, it is easy to see that not many things could have gone wrong. 
Something must be wrong with either the value of y or x. The most likely scenario is that the value of x is zero,
and that the program is therefore attempting to divide by zero. We can test this by asking the debugger to display the value of x:

```
(gdb) display x
 1: x = 0 
 (gdb)
```
As we suspected, x has a value of zero. Now we can review the code to determine whether this
was intended or whether we have an implementation error. For example, we might not have intended 
the loop to run until x>=0, and instead intended it to run until x>0.


Dividing by zero is not the only thing that can cause a program to crash. 
Perhaps the most common error resulting in a crash occurs when using arrays or pointers. 
For example, suppose the following code is stored in a file called crash2.c:

```c


	/* This program crashes.  The crash point and reason for crashing
	** can be quickly identified using a debugger.  */

#include <stdio.h>

main()
{
int x,y,z[3];

y=54389;
for (x=10; x>=1; x--)
  z[y]=y/x;
printf("%d\n",z[0]);
}

```

When we compile and execute this code, the program crashes:

```
90cos@lesson> gcc -o crash2 crash2.c 
90cos@lesson>crash2 Segmentation fault (core dumped) 
90cos@lesson>
```
Using the debugger, we run the program until it crashes to find out where the problem occurred:

```
90cos@lesson> gcc -g -o crash2 crash2.c 
90cos@lesson> gdb crash2 (gdb) run 
Starting program: /home/ahoover/crash2 
Program received signal SIGSEGV, Segmentation fault. 
0x080484a2 in main () at crash2.c:10
10	z[y]=y/x; 
(gdb)

```

A “segmentation fault” is usually a bad memory access; in other words, 
the pro- gram has tried to access a memory location that does not belong to the program. 
For example, an array has a specified size. Trying to access a cell index outside the specified size is a bad memory access.
Looking at the line of code where the program crashed, we can see an access to the array z[] at cell index y.
We can ask the debugger for the value of y and compare it against the allowed range (z[] was defined as 
a three-element array, so the allowed range is 0 . . . 2):

```
(gdb) display y
1: y = 54389
(gdb)
```
As we suspected, the value for y is outside the allowed range for indices for the array z[]. 
Once again, we have quickly identified the point where the program has misbehaved and can now 
go about the process of determining if the program logic or implementation is at fault.
Using a debugger to discover where a program is crashing is probably the most popular use for a debugger. 
During program development, if a crash is observed, the first action should almost always be to run the program in a debugger to locate the problem.
























