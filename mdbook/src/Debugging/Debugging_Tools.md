## Debugging Tools:
Debugging tool is a computer program that is used to test and debug other programs. A lot of public domain software like gdb and dbx are available for debugging. They offer console-based command line interfaces. Examples of automated debugging tools include code based tracers, profilers, interpreters, etc.
Some of the widely used debuggers are:

* GNU debugger
* Valgrind
* WinDbg
* Radare2
* Visual Studio Code/Community