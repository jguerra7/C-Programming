# Debugging Performance Labs

* 1. Debug the following code by compiling it for debugging and executing it within a debugger. At which line of code does the program crash? Why does it crash there?

```c


	/* This is an exercise for debugging. */

	/* This code has a compile-time error, and at
	** least one run-time error.  */

#include <stdio.h>
#include <math.h>

main(int argc,char *argv[])
{
int n,i;
int d2,count,
double d1;

while (1)
  {
  printf("Enter a number (0 to quit): ");
  scanf("%d",&n);
  if (n == 0)
    break;
  count=0;
  for (i=0; i<n; i++)
    {
    d1=(double)n/(double)i;
    d2=n/i;
    if (fabs(d1-(double)d2) < 0.00001)
      count++;
    }
  if (count == 2)
    printf("%d is prime\n",n);
  else
    printf("%d is not prime\n",n);
  }
}
```
* 2. Use gdb to track down the error in the following code.
```c
#include <stdio.h>

int main (void)
{
     const int  data[5] = {1, 2, 3, 4, 5};
     int  i, sum;

     for (i = 0; i >= 0; ++i)
         sum += data[i];

     printf ("sum = %i\n", sum);

     return 0;
}
```
```
Your C program must be compiled with the gcc compiler using 
the -g option to make full use of gdb’s features. 
The -g option causes the C compiler to add extra information to the output file, 
including variable and structure types, source filenames, and C statement to machine code mappings.
```









