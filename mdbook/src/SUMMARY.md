# C Programming Table of Contents

- [Introduction](Introduction/README.md)
    - [Definitions](Introduction/Definitions.md)
    - [Features](Introduction/features.md)
    - [C Specifics](Introduction/C-specifics.md)
    - [GCC](Introduction/gcc.md)
    - [CMake](Introduction/cmake.md)
- [Variables](Variables/README.md)

    - [Coding Style Guide](Variables/Coding_style_guide.md)
    - [Variable Names](Variables/Variable_names.md)
    - [Data Types](Variables/Data_types.md)
    - [Sizes](Variables/Sizes.md)
    - [Declarations](Variables/Declarations.md)
        - [Performance Lab](Variables/performance_labs/lab1.md)  
    - [Keywords](Variables/Keywords.md)
    - [Type Conversions](Variables/Type_conversions.md)
        - [Performance Lab](Variables/performance_labs/lab2.md) 
- [Arrays & Strings](Arrays_strings/README.md)

    - [Coding Style guide](Arrays_strings/Coding_style_guide.md)
    - [Arrays](Arrays_strings/Arrays.md)
    - [Access & Modify Arrays](Arrays_strings/access_modify_Arrays.md)
        - [Performance Lab](Arrays_strings/performance_labs/Arrays_Perf_labs.md)
    - [Strings](Arrays_strings/strings.md)
        - [Performance Lab](Arrays_strings/performance_labs/Strings_Perf_labs.md) 
    - [String Methods](Arrays_strings/string_methods.md)
- [I.O Part 1](IO_part_1/Objectives.md)

    - [C Streams](IO_part_1/c-streams.md)
    - [Functions](IO_part_1/functions.md)
    - [GetChar & PutChar](IO_part_1/getchar-putchar.md)
        - [Performance Lab](IO_part_1/performance_labs/lab5/lab5.md)
    - [GetC & PutC](IO_part_1/getc-putc.md)
        - [Performance Lab](IO_part_1/performance_labs/lab6/lab6.md) 
    - [String IO](IO_part_1/string-io.md)
        - [Performance Lab](IO_part_1/performance_labs/lab7/lab7.md) 
    - [Printf](IO_part_1/printf.md)
    - [scanf](IO_part_1/scanf.md)
        - [Performance Lab](IO_part_1/performance_labs/lab8/lab8.md) 
    - [fprintf & fscanf](IO_part_1/fprintf-fscanf.md)
- [Operators & Expressions](Operators_expressions/README.md)

    - [Definitions](Operators_expressions/definitions.md)
    - [Arithmetic Operators](Operators_expressions/arithmetic-operators.md)
    - [Relational Operators](Operators_expressions/relational-operators.md)
    - [Logical Operators](Operators_expressions/logical-operators.md)
    - [Assignment Operators](Operators_expressions/assignment-operators.md)
    - [Precedence](Operators_expressions/precedence.md)
        - [Performance Lab](Operators_expressions/performance_labs/Lab9.md)
        - [Performance Check](Operators_expressions/performance_labs/01_Perf_labs.md)
- [Bitwise Operations](Bitwise_operators/README.md)
    - [Bitwise Operations](Bitwise_operators/bitwise-operations.md)
    - [Numbering Systems](Bitwise_operators/numbering-systems.md)
    - [Bitwise Operators](Bitwise_operators/bitwise-operators.md)
        - [Performance Lab](Bitwise_operators/performance_labs/Perf_labs.md)
        - [Performance Lab](Bitwise_operators/performance_labs/lab10.md)  
        - [Performance Lab](Bitwise_operators/capstone-1.md) 
- [Control Flow](Control_flow/README.md)

    - [Statements & Blocks](Control_flow/statements-blocks.md)
    - [if Statements](Control_flow/conditional-statements.md)
        - [Performance Lab](Control_flow/performance_labs/Lab11.md) 
    - [if-else](Control_flow/if-else.md)
        - [Performance Lab](Control_flow/performance_labs/Lab12.md)
    - [else-if](Control_flow/else-if.md)
        - [Performance Lab](Control_flow/performance_labs/Lab13.md)
    - [switch statements](Control_flow/switch.md)
        - [Performance Lab](Control_flow/performance_labs/Lab14.md)
    - [Loops](Control_flow/loops.md)
        - [Performance Lab](Control_flow/performance_labs/Lab15.md)
    - [while loops](Control_flow/while-loops.md)
        - [Performance Lab](Control_flow/performance_labs/Lab16.md)
    - [do while loops](Control_flow/do-while.md)
        - [Performance Lab](Control_flow/performance_labs/Lab17.md)
    - [Break](Control_flow/break.md)
        - [Performance Lab](Control_flow/performance_labs/Lab18.md) 
    - [Nested Control Flow](Control_flow/nested-control-flow.md)
        - [Peformance Lab](Control_flow/performance_labs/Lab19.md) 
- [Functions](Functions/README.md)

    - [Functions](Functions/functions.md)
    - [Function Basics](Functions/functions_bacics.md)
        - [Performance Lab](Functions/performance_labs/lab8A/Lab20.md)
    - [Scope Rules](Functions/scope_rules.md)
    - [Storage Class Specifiers](Functions/storage_class_specifiers.md)
    - [Header Files](Functions/header_files.md)
    - [Recursion](Functions/recursion.md)
        - [Performance Labs](Functions/performance_labs/Perf_labs.md)
- [C Compiler](C_compiler/README.md)

    - [C Compiler General](C_compiler/compiler_general.md)
    - [Compilation Process](C_compiler/compilation_process.md)
        - [Performance Lab](C_compiler/.md)
    - [Manual Linking](C_compiler/manual_linking.md)
        - [Performance Lab](C_compiler/demonstration_labs/manual_linking.md) 
- [Preprocessor](Preprocessor/README.md)

    - [Coding Style Guide](Preprocessor/coding_style_guide.md)
    - [Stub Code](Preprocessor/stub_code.md)
    - [Directives](Preprocessor/directives.md)
        - [Performance Lab](Preprocessor/performance_labs/lab24.md) 
    - [Hash Operator](Preprocessor/hash_operator.md)
    - [Double Hash](Preprocessor/double_hash.md)
    - [Undef](Preprocessor/undef.md)
    - [Conditional Compilation](Preprocessor/conditional_compilation.md)
        - [Performance Lab](Preprocessor/performance_labs/Lab25.md) 
- [Pointers & Arrays part 2](Pointers_Arrays/README.md)

    - [Coding Style Guide](Pointers_Arrays/coding_style_guide.md)
    - [Definitions](Pointers_Arrays/definitions.md)
    - [Endianness](Pointers_Arrays/endianness.md)
    - [Memory Visualizaion](Pointers_Arrays/memory_visualization.md)
    - [Memory Operators](Pointers_Arrays/Memory_Operators.md)
        - [Performance Lab](Pointers_Arrays/performance_labs/PerfLab_Mem_Op.md)
    - [Arrays Review](Pointers_Arrays/Arrays_Review.md)
    - [Address Arithmetic](Pointers_Arrays/Address_Arithmetic.md)
        - [Performance Lab](Pointers_Arrays/performance_labs/PerfLab_string_Splitter.md) 
    - [Function Arguments](Pointers_Arrays/functuion_Arguments.md)
        - [Performance Lab](Pointers_Arrays/performance_labs/surfin_bird.md)
    - [Pointer Arrays](Pointers_Arrays/Pointer_Arrays.md)
    - [Multi-Dimensional Arrays](Pointers_Arrays/Multi_Dim_Arrays.md)
        - [Performance Arrays](Pointers_Arrays/performance_labs/tic_tac_toe.md) 
    - [Funtion Pointers](Pointers_Arrays/function_pointers.md)
        - [Performance Arrays](Pointers_Arrays/performance_labs/texas.md) 
    - [Double Pointers](Pointers_Arrays/double_pointers.md)
- [I.O part 2](IO_part_2/README.md)

    - [File Data Type](IO_part_2/file_data_type.md)
    - [Related Functions](IO_part_2/related_functions.md)
        - [Performance Lab Mighty Morphin](IO_part_2/performance_labs/PL_mighty_morphin.md)
        - [Performance Lab Your Song](IO_part_2/performance_labs/PL_your_song.md)
        - [Performance Lab Content Copy](IO_part_2/performance_labs/PL_content_copy_line.md)
        - [Performance Lab User Names](IO_part_2/performance_labs/PL_usernames.md)
- [Memory Management](Memory_Management/concepts.md)

    - [Stack Based Memory](Memory_Management/stack_memory.md)
    - [Heap Based Memory](Memory_Management/heap_memory.md)
    - [C Standard Library](Memory_Management/c_standard_lib.md)
        - [Performance Lab Haystack Needle](Memory_Management/performance_labs/Lab_Haystack_Needle.md)
- [Structs](Structs/README.md)

    - [Structs](Structs/structs.md)
    - [Struct Format](Structs/structs_format.md)
    - [Arrays of Structs](Structs/arrays_of_structs.md)
    - [Struct Visualization](Structs/struct_visualization.md)
        - [Performance Lab Surfin Bird](Structs/performance_labs/Lab_surfinbird.md)
    - [Linked Lists](Structs/linked_lists.md)
        - [Performance Lab](Structs/performance_labs/Lab_Linked_list.md) 
    - [Functions Pointers](Structs/function_pointers.md)
- [Error Handling](Error_handling/README.md)

    - [Assert](Error_handling/assert.md)
        - [Performance Lab](Error_handling/Performance_Labs/Lab_string.md) 
    - [Errno](Error_handling/errno.md)
        - [Performance Lab](Error_handling/Performance_Labs/Lab_errno.md)
    - [Assert & Errno](Error_handling/assert_vs_errno.md)
        - [Performance Lab](Error_handling/Performance_Labs/Lab_output.md) 
    - [Math Functions](Error_handling/Math_functions.md)
    - [Pointers](Error_handling/pointers.md)
    - [Strings & Files](Error_handling/strings_and_files.md)
        - [Collaborative Project](Error_handling/Performance_Labs/project.md) 

-----------

[Contributors](misc/contributors.md)
