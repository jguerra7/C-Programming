# Introduction to C Programming

**The purpose of this topic is to familiarize the student with key information that they will
use to understand terminology and feature of C programming, and setup their working
environment.**

## Topics:

* ### Terminology
* ### Language Features
* ### C Specifics
* ### Setting up the Environment

## By the end of this lesson you should know:

* How to setup your C programming environment on both Windows and Linux
* The basics of the different text editors and development environments \(Sublime Text, Visual Studio, Visual Studio Code, Atom, Nano, Vi/VIM\)
* Some basic programming definitions such as:
  * IDE
  * Pseudocode
  * Scope
  * Language Level
  * Whitespace
  * Function
  * Statement
  * Block
* The general and specific features to the C Language \(on a basic level\) such as:
  * main\(\)
  * types
  * pointers
  * filenames
  * comments
